<?php

use yii\db\Migration;

/**
 * Class m171121_001928_languages
 */
class m171121_001928_languages extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('languages', [
            'id' => $this->primaryKey(),
            'alias' => 'string NOT NULL',
            'value' => 'string NOT NULL',
            'icon' => 'string DEFAULT NULL',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171121_001928_languages cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171121_001928_languages cannot be reverted.\n";

        return false;
    }
    */
}
