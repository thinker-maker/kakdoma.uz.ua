<?php

use yii\db\Migration;

class m171109_112450_static extends Migration
{
    public function safeUp()
    {
        $this->createTable('static_pages',[
            'static_page_id' => $this->primaryKey(),
            'static_page_alias' => 'TINYTEXT NOT NULL',
            'static_page_name' => 'TINYTEXT NOT NULL',
            'static_page_full_text' => 'TEXT NULL',
            'static_page_meta_description' => 'TINYTEXT NOT NULL',
            'static_page_keywords' => 'TINYTEXT NOT NULL',
            'static_page_show' => 'TINYINT(1) NOT NULL DEFAULT 1',
            'static_page_in_footer' => 'TINYINT(1) NOT NULL DEFAULT 1',
        ]);
    }

    public function safeDown()
    {
        echo "m171109_112450_static cannot be reverted.\n";

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171109_112450_static cannot be reverted.\n";

        return false;
    }
    */
}
