<?php

use yii\db\Migration;

/**
 * Class m171225_173220_ap_attributes
 */
class m171225_173220_ap_attributes extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('attributes', [
            'id' => $this->primaryKey(),
            'name' => 'string NOT NULL',
            'img' => 'string',
            'active' => 'int(1) DEFAULT 1',
        ]);
        
        $this->addColumn('apartments', 'attribs', 'string');
        
        $this->addColumn('apartments', 'active', 'int(1)');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171225_173220_ap_attributes cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171225_173220_ap_attributes cannot be reverted.\n";

        return false;
    }
    */
}
