<?php

use yii\db\Migration;

/**
 * Class m171109_160518_feedsettings
 */
class m171109_160518_feedsettings extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('settings', [
            'id' => 1,
            'name' => 'show_prices',
            'value' => 'true',
            ]);
        
        $this->insert('settings', [
            'id' => 2,
            'name' => 'show_discount_label',
            'value' => 'true',
            ]);
        
        $this->insert('settings', [
            'id' => 3,
            'name' => 'main_phone_number',
            'value' => '+38(012)345-67-89',
            ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171109_160518_feedsettings cannot be reverted.\n";

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171109_160518_feedsettings cannot be reverted.\n";

        return false;
    }
    */
}
