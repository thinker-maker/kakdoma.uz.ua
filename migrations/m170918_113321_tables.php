<?php

use yii\db\Migration;

class m170918_113321_tables extends Migration
{
    public function safeUp()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'login' => 'string NOT NULL',
            'password' => 'string NOT NULL',
        ]);
        
        $this->createTable('apartments', [
            'id' => $this->primaryKey(),
            'description' => 'text',
            'address' => 'string',
            'coordinates' => 'string',
            'price' => 'int(5)',
            'room_count' => 'int(1)',
        ]);
        
        $this->createTable('apartment_pictures', [
            'id' => $this->primaryKey(),
            'apt_id' => 'int(3)',
            'name' => 'string',
        ]);
        
        $this->addForeignKey('picture_owner', 'apartment_pictures', 'apt_id', 'apartments', 'id', 'CASCADE', 'CASCADE');
        
        $this->createTable('settings', [
            'id' => $this->primaryKey(),
            'name' => 'string',
            'value' => 'string',
        ]);
        
        $this->createTable('apartment_labels', [
            'id' => $this->primaryKey(),
            'apt_id' => 'int(3)',
            'name' => 'string',
            'show' => 'int(1)',
        ]);
        
        $this->addForeignKey('label_owner', 'apartment_labels', 'apt_id', 'apartments', 'id', 'CASCADE', 'CASCADE');
        
    }

    public function safeDown()
    {
        echo "m170918_113321_tables cannot be reverted.\n";

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170918_113321_tables cannot be reverted.\n";

        return false;
    }
    */
}
