<?php

use yii\db\Migration;

/**
 * Class m171121_001912_phones
 */
class m171121_001912_phones extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('phones', [
            'id' => $this->primaryKey(),
            'number' => 'string NOT NULL',
            'operator_icon' => 'string DEFAULT NULL',
            'show_number' => 'int DEFAULT NULL',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171121_001912_phones cannot be reverted.\n";

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171121_001912_phones cannot be reverted.\n";

        return false;
    }
    */
}
