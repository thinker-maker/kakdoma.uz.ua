<?php

namespace app\components;

use Yii;
use yii\web\Controller;

/**
 * StaticPagesController implements the CRUD actions for StaticPages model.
 */
class BaseController extends Controller
{
    public $layout = 'admin';
    
    public function beforeAction($action)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect('/site/index');
        }
        
        return parent::beforeAction($action);
    }
}