<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
//use voime\GoogleMaps\Map;
use dosamigos\tinymce\TinyMce;
use yii\helpers\Url;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\Apartments */
/* @var $form yii\widgets\ActiveForm */
?>        

<div class="apartments-form">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="row">
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-6">
                    <label class='labels'>Address</label><br>
                    <?= $form->field($model, 'address')->textInput(['maxlength' => true])->label(false) ?>
                </div>
                <div class="col-md-3">
                    <label class='labels'>Price</label><br>
                    <?= $form->field($model, 'price')->textInput()->label(false) ?>
                </div>        
                <div class="col-md-3">
                    <label class='labels'>Room number</label><br>
                    <?= $form->field($model, 'room_count')->textInput()->label(false) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'description')->widget(TinyMce::className(), [
                        'options' => ['rows' => 12],
                        'language' => 'ru',
                        'clientOptions' => [
                            'plugins' => [
                                "advlist autolink lists link charmap print preview anchor",
                                "searchreplace visualblocks code fullscreen",
                                "insertdatetime media table contextmenu paste"
                            ],
                            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
                        ]
                    ])->label(false);?>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <label class="col-md-12 labels">Attributes</label><br>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?php
                        for($i = 0; $i < count($attributes); $i++){
                            if(isset($attributes[$i])){
                                $selected = $attributes[$i]["on"] ? '' : '-not';
                                echo '<span class="attr-select col-sm-3">'
                                . '<img class="attribute'. $selected .'-selected" onclick="setValue();" src="'. Url::home(true) . 'images/attributes/' . $attributes[$i]["img"].'" />'
                                .'<input type="hidden" class="attr-selected" name="image['.$attributes[$i]["id"].']" value="'.$attributes[$i]["on"].'">'
                                .'</span>';
                            }
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= FileInput::widget([
                'name' => 'Apartments[image][]',
                'options' => [
                    'multiple' => true,
                    'accept' => 'image/*',
                ],
                
            ]);
            ?>
        </div>
    </div><br>
    <?php
        if(is_array($model["images"])){
            echo '<div class="row"><div class="col-sm-12">';
            foreach($model["images"] as $img){
                echo '<img class="col-sm-3" src="'. Url::home(true) . 'images/apartments/' . $model["id"].'/'. $img["name"] .'" />';
            }
            echo '</div></div><br>';
        }
    ?>
    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'lat')->textInput($model["lat"] != 0 ? ["value" => $model["lat"]] : ["placeholder" => 'Широта', "value" => ''])->label(false) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'lon')->textInput($model["lat"] != 0 ? ["value" => $model["lon"]] : ["placeholder" => 'Довгота', "value" => ''])->label(false) ?>
        </div>
        <div class="col-md-2">
            <?= Html::button('Check on map', [ 'class' => 'btn btn-primary', 'onclick' => '(initMap(Number($("#apartments-lat").val()), Number($("#apartments-lon").val())))();' ]); ?>
        </div>
        <div class="col-md-2">
            <?= Html::submitButton(($model->isNewRecord ? 'Create' : 'Update').' apartment', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'active')->checkbox()->label('Show on site') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div id="map"></div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<script>
    function initMap(lt = 48.6216995, ln = 22.2981629) {
            var uluru = {lat: lt, lng: ln};
            var map = new google.maps.Map(document.getElementById('map'), {
              zoom: 17,
              center: uluru,
              mapTypeId: 'satellite'
            });
            var marker = new google.maps.Marker({
              position: uluru,
              map: map
            });
          }

    function setValue(){
        var element = $(event.target).closest('span').find('.attr-selected');
        if(element[0].value > 0){ 
            element[0].value = 0;
            $(event.target).attr('class', 'attribute-not-selected');
        }
        else{
            element[0].value = 1;
            $(event.target).attr('class', 'attribute-selected');
        }
    }
</script>
<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=<?= Yii::$app->params['GOOGLE_API_KEY']?>&callback=initMap">
</script>