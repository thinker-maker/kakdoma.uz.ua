<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\Url;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\models\Phones */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="phones-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        
        <div class="col-sm-4">
            <?= $form->field($model, 'number')->textInput(['maxlength' => true])->label(false) ?>
        </div>
        <div class="col-sm-1"></div>
        <div class="col-sm-4">
            <?= $form->field($model, 'show_number', ['options' => ['class' => 'xform-group']])->checkbox([
                'uncheck'      => null,
                'class'        => 'chk',
                'id'           => 'model-category_id_' . $category->id,
                'label'        => $category->name,
                'labelOptions' => ['class' => 'chk full-width'],
                'template'     => '{input}{beginLabel}{labelTitle}{endLabel}{error}',
                'value'        => $category->id,
                'checked'      => true,
            ]);?>
            <input type="checkbox" id="switch" /><label for="switch">Show number on site</label>
        </div>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-2">
            <input type="checkbox" checked="true" id="setIcon"><label for="setIcon">Set icon</label>
        </div>
    </div>
    <br>
    <div id="show">
        <label class="control-label">Select operator icon from existing or upload new</label>
        <div class="row">
            <div class="col-sm-4" id="selector">
                <ul class="prod-gram">
                    <li class="init"><b>&#11167;</b> Select</li>
                    <?php foreach($icons as $icon){
                        echo '<li>'.$icon.'</li>';
                    }?>
                </ul>
            </div>
            <div class="col-sm-6">
                <?= $form->field($model, 'operator_icon')->widget(FileInput::classname(), [
                        'options'=>['accept'=>'image/*'],
                        'pluginOptions'=>['allowedFileExtensions'=>['jpg','gif','png']]
                    ])->label(false);
                    //Url::to(['/images/site/mobile']),
                ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
    <br><br>
    <?php /*$form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'operator_icon')->textInput(['maxlength' => true]) ?>

    

    <?php ActiveForm::end(); */?>

</div>
<script>
    window.onload = function() {
        $(document).on("click", "ul.prod-gram .init", function() {
          $(this).parent().find('li:not(.init)').toggle();
        });
        var allOptions = $("ul.prod-gram").children('li:not(.init)');
        $("ul.prod-gram").on("click", "li:not(.init)", function() {
          allOptions.removeClass('selected');
          $(this).addClass('selected');
          $(this).parent().children('.init').html($(this).html());
          $(this).parent().find('li:not(.init)').toggle();
        });
        
        $('#setIcon').click(function() {
            if( $(this).is(':checked')) {
                $("#show").show();
            } else {
                $("#show").hide();
            }
        });
      };
</script>