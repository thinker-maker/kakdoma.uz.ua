<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php foreach ($models as $model):?>
    <div class="row">
        <div class="col-sm-6">
            <?= $items[$model->id]
            ?>
        </div>
        <div class="col-sm-6">
            <div class="row">
                <h2><?= $model->address?></h2>
            </div>
            <div class="row">
                <h4><?= strlen($model->description) > 200 ? substr($model->description, 0, 200) . '... <a href="'. Yii::$app->homeUrl . 'site/apartment?id=' . $model->id .'">Читать дальше</a>' : $model->description?></h4>
            </div>
            <div class="row">
                <h4><?= isset($attributeLinks[$model->id])? $attributeLinks[$model->id] : ''?></h4>
            </div>
        </div>
    </div>
    <br><br>
<?php endforeach;?>
