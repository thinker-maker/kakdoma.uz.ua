<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use voime\GoogleMaps\Map;

$this->title = $model->address;
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?=$model->address?></h1>
<div class="row">
    <?= $model->description ?>
</div><br><br>
<div class="row">
    <?= $attributeLinks ?>
</div><br><br>
<div class="row">
    <?php
        $latlon = explode(',', $model->coordinates);
        $coordinates = [isset($latlon[0]) ? $latlon[0] : 48.6143193, isset($latlon[1]) ? $latlon[1] : 22.2681299];
        echo Map::widget([
            'width' => '100%',
            'height' => '250px',
            'zoom' => 17,
            'center' => $coordinates,
            'mapType' => Map::MAP_TYPE_HYBRID,
            'markers' => [
                    ["position" => $coordinates,]
                ]
            //'markerFitBounds'=>true
        ]);
    ?>
</div><br><br>
<div class="row apartment-fullsize">
    <img id="large" class="apartment-full-size-image" src="<?= \Yii::getAlias('@web'). '/images/apartments/'.$model->image?>"/>
    <?php foreach($items as $item):?>
        <div class="col-sm-4">
            <?= $item?>
        </div>
    <?php endforeach;?>
</div>
<script>
    function setImage(){
        $(".apartment-carousel-image-selected").attr("class", "apartment-carousel-image");
        $(event.target).attr("class", "apartment-carousel-image-selected");
        $('#large').attr("src", $(event.target)[0].src);
    }
</script>

