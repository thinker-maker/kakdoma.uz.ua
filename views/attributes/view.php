<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Attributes */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Attributes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="attributes-view">

    <h1><?= 'Record ' . ($model->isNewRecord ? 'created' : 'updated') ?></h1>
    <br>
    <div class="row">
        <div class="col-sm-1">
            <img src="<?= Url::home(true) . 'images/attributes/' . $model->img ?>">
            <br><br>
        </div>
        <div class="col-sm-2 attribute">
            <label><?= $model->name?></label>
        </div>
    </div>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

</div>
