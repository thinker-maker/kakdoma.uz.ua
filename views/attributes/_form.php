<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Attributes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="attributes-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <div class="row">
        <div class="col-sm-2">
            <label>Name</label>
            <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label(false) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-2">
            <?=
                $model->isNewRecord ? '' :
                    '<label>Current image:</label><br>'.
                    '<img src="'. Url::home(true) . 'images/attributes/' . $model->img.'">'.
                    '<br><br>'
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <?= '<label>'.
                ($model->isNewRecord ? 
                'Set' :
                'Change') .
                ' image:</label><br>'
            ?>
            <?= $form->field($model, 'image')->widget(FileInput::classname(), [
                    'options'=>['accept'=>'image/*'],
                    'pluginOptions'=>['allowedFileExtensions'=>['jpg','gif','png', 'jpeg']]
                ])->label(false);
            ?>
        </div>        
    </div>
    <div class="row">
        <div class="col-sm-3">
            <?= $form->field($model, 'active')->checkbox()->label(false) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-2">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
    
    <?php ActiveForm::end(); ?>

</div>
