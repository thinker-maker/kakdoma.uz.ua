<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;
use app\models\Settings;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<?php $menuItems = [
    'about' => 'О нас', 
    'apartments' => 'Квартиры', 
    'reservation' => 'Бронирование',
    'contacts' => 'Контакты'
    ]
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    <div class="container col-sm-8">
        <div class="row">            
            <div class="logo col-sm-4" style="height:130px">
                <div style="margin-top:60px;height:50px">
                    <a href="/" class="h40px"><?= \Yii::$app->params['siteName']?></a>
                </div>
                <div style="margin-top:-10px;height:30px">
                    <span>Оренда квартир. Посуточно.</span>
                </div>
            </div>
            <div style="margin-top:10px">
                <div class="phone">
                        <p>
                            <span>Номер телефона по Украине:</span><br>
                            <a href="/read/contacts">+38<span>(050)<b>432-27-44</b></span></a>
                        </p>
                </div>
                <div class="menu-top" style="margin-top:30px">
                    <ul>
                        <?php foreach($menuItems as $key => $value): ?>
                            <li class="<?php echo \Yii::$app->controller->action->id == $key ? 'current' : ''?>"><a href="/site/<?= $key ?>"><?= $value ?></a></li>
                        <?php endforeach; ?>
                        <?php if(!\Yii::$app->user->isGuest): ?>
                            <li class="to-admin-panel"><?= Html::a('Admin panel', ['admin/index'], ['data' => ['method' => 'post']]) ?></li>
                        <?php endif;?>
                    </ul>
                </div>
            </div>
        </div>
        <?php $this->beginBody() ?>
            <div class="wrap" style="margin-top:20px">             
                <?= $content ?>        
            </div>
        <?php $this->endBody() ?>
    </div>
    <footer class="footer container col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <a href="/">&copy; <?= \Yii::$app->params["siteName"].'. 2009 - '. date('Y') ?></a>
            </div>
            <div class="col-xs-3">
                <a href="http://7rl.com.ua/site/portfolio" target="_blank" style="color:#AADBFF">7RL</a>
            </div>
        </div>
    </footer>
    <?php
        $show_label = Settings::find()->where(['=', 'name', 'show_discount_label'])->one();
        if($show_label["value"] == 'true'){
            echo '<a href="/site/contacts" class="special-offer" '
            . 'title="Скидки при поселении от 3-х дней и более!">'
            . '<img src="'. \Yii::getAlias("@web").'/images/site/discount_labels/special-offer.png" />'
            . '</a>';
        }
    ?>
</body>
</html>
<?php $this->endPage() ?>
