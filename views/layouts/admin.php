<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<?php $menuItems = ['item1', 'item2', 'item3']?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="body-admin">
    <div class="container container-admin col-sm-9">
        <div class="row col-sm-offset-1">            
            <div class="logo col-sm-4" style="height:130px">
                <div style="margin-top:60px;height:50px">
                    <a href="/" class="h40px">Admin panel</a>
                </div>
            </div>
            <div style="margin-top:10px">
                <div class="menu-top">
                    <ul>
                        <li class="current"><a href="#">menu 1</a></li>
                        <?php foreach($menuItems as $key => $value): ?>
                        <li><a href="#"><?= $value ?></a></li>
                        <?php endforeach; ?>
                        <?php if(!\Yii::$app->user->isGuest): ?>
                            <li><?= Html::a('Logout', ['admin/logout'], ['data' => ['method' => 'post']]) ?></li>
                        <?php endif;?>
                    </ul>
                </div>
            </div>
        </div>
        <?php $this->beginBody() ?>
        <div class="wrap">
            <div style="margin-top:20px;margin-left:20px">                
                <?= $content ?>
            </div>
        
        <footer>
            <div>
                <p class="footer col-xs-5"><a href="/"><?= \Yii::$app->params["siteName"].'. 2009 - '. date('Y') ?></a></p>
            </div>
        </footer></div>
        </div>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
