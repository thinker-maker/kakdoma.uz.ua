<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Static Pages';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="static-pages-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Static Pages', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'static_page_id',
            'static_page_alias:ntext',
            'static_page_name:ntext',
            'static_page_full_text:ntext',
            'static_page_meta_description:ntext',
            // 'static_page_keywords:ntext',
            // 'static_page_show',
            // 'static_page_in_footer',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
