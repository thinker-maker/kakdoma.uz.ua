<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\StaticPages */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="static-pages-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'static_page_alias')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'static_page_name')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'static_page_full_text')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'static_page_meta_description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'static_page_keywords')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'static_page_show')->textInput() ?>

    <?= $form->field($model, 'static_page_in_footer')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
