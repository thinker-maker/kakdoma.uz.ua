<?php

namespace app\controllers;

use Yii;
use app\models\Apartments;
use app\models\Attributes;
use app\models\ApartmentPictures;
use yii\web\UploadedFile;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\FileHelper;
use app\components\BaseController;

/**
 * ApartmentsController implements the CRUD actions for Apartments model.
 */
class ApartmentsController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Apartments models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Apartments::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Apartments model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Apartments model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Apartments();
        $attributes = Attributes::find()->where(['=', 'active', 1])->all();
        if ($model->load(Yii::$app->request->post())) {
            $model->coordinates = $model->lat. ',' .$model->lon;
            $attributes_selected = Yii::$app->request->post("image");
            if(is_array($attributes_selected) && !empty($attributes_selected)){
                foreach($attributes_selected as $key => $selected){
                    $model->attribs .= $selected == 1 ? $key.',' : '';
                }
            }
            if($model->save()){
                $images = UploadedFile::getInstancesByName('Apartments[image]');
                $path = Yii::$app->basePath . '/web/images/apartments/' .$model->id;
                if (FileHelper::createDirectory($path, $mode = 0775, $recursive = true)) {
                    foreach($images as $image){
                        $pic = new ApartmentPictures();
                        $pic->apt_id = $model->id;
                        $pic->name = $image->name;
                        $pic_path = $path . '/' . $pic->name;
                        if($image->saveAs($pic_path)){
                            $pic->save();
                        }
                    }
                }
                //$image->saveAs($path);
                return $this->redirect('index');
            }
        }
        return $this->render('create', [
            'model' => $model,
            'attributes' => $attributes,
        ]);
    }

    /**
     * Updates an existing Apartments model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $attributes = Attributes::find()->where(['=', 'active', 1])->all();
        $current_attribs = explode(',', $model->attribs);
        if(is_array($current_attribs) && !empty($current_attribs)){
            foreach($current_attribs as $current_attrib){
                foreach($attributes as $attribute){
                    if($attribute->id == $current_attrib){
                        $attribute->on = 1;
                        break;
                    }
                }
            }
        }
        $model->images = ApartmentPictures::find()->where(['=', 'apt_id', $id])->all();
        $latlon = explode(',', $model->coordinates);
        $model->lat = $latlon[0];
        $model->lon = $latlon[1];
        if ($model->load(Yii::$app->request->post())) {
            $model->coordinates = $model->lat. ',' .$model->lon;
            $attributes_selected = Yii::$app->request->post("image");
            if(is_array($attributes_selected) && !empty($attributes_selected)){
                $model->attribs = '';
                foreach($attributes_selected as $key => $selected){
                    $model->attribs .= $selected == 1 ? $key.',' : '';
                }
            }
            if($model->save()){
                $images = UploadedFile::getInstancesByName('Apartments[image]');
                $path = Yii::$app->basePath . '/web/images/apartments/' .$model->id;
                if (FileHelper::createDirectory($path, $mode = 0775, $recursive = true)) {
                    foreach($images as $image){
                        $pic = new ApartmentPictures();
                        $pic->apt_id = $model->id;
                        $pic->name = $image->name;
                        $pic_path = $path . '/' . $pic->name;
                        if($image->saveAs($pic_path)){
                            $pic->save();
                        }
                    }
                }
                return $this->redirect('index');
            }
        }
        return $this->render('update', [
            'model' => $model,
            'attributes' => $attributes,
        ]);
    }

    /**
     * Deletes an existing Apartments model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Apartments model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Apartments the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Apartments::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
