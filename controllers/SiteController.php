<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Apartments;
use app\models\ApartmentPictures;
use app\models\Attributes;
use app\models\Settings;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
//                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $apartments = Apartments::find()->where(['=', 'active', '1'])->all();
        $markers = [];
        $items = [];
        $show_prices = Settings::find()->where(['=', 'name', 'show_prices'])->one();
        foreach($apartments as $apartment){
            $latlon = explode(",", $apartment["coordinates"]);
            if($latlon[0] && $latlon[1] != null){
                $markers[]['position'] = [$latlon[0], $latlon[1]];
            }
            $img = ApartmentPictures::find()->where(['=', 'apt_id', $apartment["id"]])->one();
            $attributes = explode(",", $apartment["attribs"]);
            $attributeLinks = '';
            foreach($attributes as $attribute){
                if($attribute){
                    $attr = Attributes::findOne($attribute);
                    $attributeLinks .= '<img src="'.\Yii::getAlias('@web').'/images/attributes/'. $attr["img"] .'"/>';
                }
            }
            $price = $show_prices['value'] ? '<br><h4>Price: '. $apartment["price"] .'</h4>' : ''; 
            $items[]["content"] = '<div class="img-responsive carousel">'
                    .'<div id="center" class="col-sm-6 carousel-item">'
                        .'<img class="img" src="'.\Yii::getAlias('@web').'/images/apartments/'. $apartment["id"] .'/'. $img["name"] .'"/>'
                    .'</div>'
                    .'<div class="col-sm-6 carousel-item">'
                        .'<p><h1>'. $apartment["address"] .'</h1></p>'
                        .'<p>'. $attributeLinks .'</p>'
                        . $price
                    .'</div>'
                . '</div>';
        }
        return $this->render('index',[
            'markers' => $markers,
            'items' => $items,
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect('/admin/index');
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect('/admin/index');
        }
        else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        
        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContacts()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('index');
    }
    
    public function actionApartments()
    {
        $items = [];
        $models = Apartments::find()->all();
        $attributeLinks = [];
        foreach($models as $model){
            if(!$model->apartmentPictures){
                $items[$model->id] = '<img class="apartments-carousel-image" src="'.\Yii::getAlias('@web').'/images/apartments/no_image.png"/>';
                continue;
            }
            $items[$model->id] = '<img class="apartments-carousel-image" src="'.\Yii::getAlias('@web').'/images/apartments/'. $model->id .'/'. $model->apartmentPictures[0]["name"] .'"/>';
            
            $attributes = explode(",", $model->attribs);
            $attributeLinks[$model->id] = '';
            foreach($attributes as $attribute){
                if($attribute){
                    $attr = Attributes::findOne($attribute);
                    $attributeLinks[$model->id] .= '<img src="'.\Yii::getAlias('@web').'/images/attributes/'. $attr["img"] .'"/>';
                }
            }
        }
        return $this->render('apartments', [
            "models" => $models,
            "items" => $items,
            'attributeLinks' => $attributeLinks,
        ]);
    }
    
    public function actionApartment()
    {
        $id = Yii::$app->request->get("id");        
        $model = Apartments::findOne($id);
        if(!$id || !$model){
            return $this->redirect('apartments');
        }
        $items = [];
        $imgs = $model->apartmentPictures;
        if(is_array($imgs) && !empty($imgs)){
            $model->image = $id .'/'.$imgs[0]["name"];
            foreach($imgs as $img){
                $items[] = '<img onclick="setImage();" class="apartment-carousel-image" src="'.\Yii::getAlias('@web').'/images/apartments/'. $id .'/'. $img["name"] .'"/>';
            }
        }
        else{
            $items[] = '';
            $model->image = 'no_image.png';
        }
        $attributes = explode(",", $model->attribs);
        $attributeLinks = '';
        foreach($attributes as $attribute){
            if($attribute){
                $attr = Attributes::findOne($attribute);
                $attributeLinks .= '<img src="'.\Yii::getAlias('@web').'/images/attributes/'. $attr["img"] .'"/>';
            }
        }
        return $this->render('apartment', [
            "model" => $model,
            "items" => $items,
            "attributeLinks" => $attributeLinks,
        ]);
    }
    
    public function actionReservation()
    {
        \Yii::$app->telegram->sendMessage([
                'chat_id' => '-302037614',
                'text' => 'test',
        ]);
        return $this->render('index');
    }
    
    
}
