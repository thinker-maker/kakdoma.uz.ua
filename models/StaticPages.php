<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "static_pages".
 *
 * @property integer $static_page_id
 * @property string $static_page_alias
 * @property string $static_page_name
 * @property string $static_page_full_text
 * @property string $static_page_meta_description
 * @property string $static_page_keywords
 * @property integer $static_page_show
 * @property integer $static_page_in_footer
 */
class StaticPages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'static_pages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['static_page_alias', 'static_page_name', 'static_page_meta_description', 'static_page_keywords'], 'required'],
            [['static_page_alias', 'static_page_name', 'static_page_full_text', 'static_page_meta_description', 'static_page_keywords'], 'string'],
            [['static_page_show', 'static_page_in_footer'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'static_page_id' => 'Static Page ID',
            'static_page_alias' => 'Static Page Alias',
            'static_page_name' => 'Static Page Name',
            'static_page_full_text' => 'Static Page Full Text',
            'static_page_meta_description' => 'Static Page Meta Description',
            'static_page_keywords' => 'Static Page Keywords',
            'static_page_show' => 'Static Page Show',
            'static_page_in_footer' => 'Static Page In Footer',
        ];
    }
}
