<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "apartment_labels".
 *
 * @property integer $id
 * @property integer $apt_id
 * @property string $name
 * @property integer $show
 *
 * @property Apartments $apt
 */
class ApartmentLabels extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'apartment_labels';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['apt_id', 'show'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['apt_id'], 'exist', 'skipOnError' => true, 'targetClass' => Apartments::className(), 'targetAttribute' => ['apt_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'apt_id' => 'Apt ID',
            'name' => 'Name',
            'show' => 'Show',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApt()
    {
        return $this->hasOne(Apartments::className(), ['id' => 'apt_id']);
    }
}
