<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "attributes".
 *
 * @property integer $id
 * @property string $name
 * @property string $img
 * @property integer $active
 */
class Attributes extends \yii\db\ActiveRecord
{
    public $image;
    public $on;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'attributes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['active'], 'integer'],
            [['name', 'img'], 'string', 'max' => 255],
            [['image'], 'safe'],
            [['image'], 'file', 'extensions'=>'jpg, gif, png, jpeg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'img' => 'Img',
            'active' => 'Active',
        ];
    }
}
