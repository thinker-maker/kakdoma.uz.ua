<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "apartments".
 *
 * @property integer $id
 * @property string $description
 * @property string $address
 * @property string $coordinates
 * @property integer $price
 * @property string $room_count
 *
 * @property ApartmentLabels[] $apartmentLabels
 * @property ApartmentPictures[] $apartmentPictures
 */
class Apartments extends \yii\db\ActiveRecord
{
    public $lat = 0;
    public $lon = 0;
    public $image;
    public $images = [];
    public $attribute;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'apartments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description', 'room_count', 'attribs', 'lat', 'lon'], 'string'],
            [['price', 'active'], 'integer'],
            [['address'], 'required'],
            [['address', 'coordinates'], 'string', 'max' => 255],
            [['image', 'images'], 'safe'],
            [['image'], 'file', 'extensions'=>'jpg, gif, png, jpeg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'description' => 'Description',
            'address' => 'Address',
            'coordinates' => 'Coordinates',
            'price' => 'Price',
            'room_count' => 'Room Count',
            'active' => '',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApartmentLabels()
    {
        return $this->hasMany(ApartmentLabels::className(), ['apt_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApartmentPictures()
    {
        return $this->hasMany(ApartmentPictures::className(), ['apt_id' => 'id']);
    }
    
    public function getAllApartments()
    {
        return $this->findAll();
    }
    
    public function getAptImages($id){
        return ApartmentPictures::find()->where(['=', 'apt_id', $id])->all();
    }
}
